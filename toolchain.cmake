set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)
SET(CMAKE_CROSSCOMPILING 1)

set(RAM_FILE "${CMAKE_BINARY_DIR}/resources/md407-ram.x")

set(CMAKE_C_COMPILER_WORKS 1)
set(CMAKE_C_COMPILER "${TOOLCHAIN_PATH}arm-none-eabi-gcc")
set(CMAKE_CXX_COMPILER "${TOOLCHAIN_PATH}arm-none-eabi-g++")
set(CMAKE_OBJCOPY "${TOOLCHAIN_PATH}arm-none-eabi-objcopy")

set(CMAKE_C_FLAGS "-g -O0 -mthumb -Wall -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -msoft-float -Wa,-adhln=test.s")

set(CMAKE_C_LINK_FLAGS "-nostartfiles -T${RAM_FILE} -nostdlib")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
